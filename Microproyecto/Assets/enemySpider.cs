using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class enemySpider : MonoBehaviour
{
    public Transform[] patrolPoints;
    public float moveSpeed;
    public int patrolDestiantion;

    // Update is called once per frame
    void Update()
    {
        if (patrolDestiantion == 0)
        {
            transform.position = Vector2.MoveTowards(transform.position, patrolPoints[0].position, moveSpeed * Time.deltaTime);
            if (Vector2.Distance(transform.position, patrolPoints[0].position) < .2f)
            {
                transform.localScale = new Vector3(1f, 1f, 1f);
                patrolDestiantion = 1;
            }
        }

        if (patrolDestiantion == 1)
        {
            transform.position = Vector2.MoveTowards(transform.position, patrolPoints[1].position, moveSpeed * Time.deltaTime);
            if (Vector2.Distance(transform.position, patrolPoints[1].position) < .2f)
            {
                transform.localScale = new Vector3(-1f, 1f, 1f);
                patrolDestiantion = 0;
            }
        }
    }
}
