using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class llaveScript : MonoBehaviour
{
    public GameObject Objetollave;
    public GameObject Puerta;

    public AudioSource audioSource;
    public AudioClip pickUp;

    private void OnTriggerEnter2D(Collider2D other)
    {
        if(other.CompareTag("Player"))
        {
            audioSource.PlayOneShot(pickUp);
            Destroy(Puerta);
            Destroy(Objetollave);
        }
    }
}
