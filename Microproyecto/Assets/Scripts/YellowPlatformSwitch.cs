using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class YellowPlatformSwitch : MonoBehaviour
{

    public GameObject YellowPlatform;
    public GameObject GreenPlatform;
    
    // Start is called before the first frame update
    void Start()
    {
        YellowPlatform.SetActive(false);
        
    }

    // Update is called once per frame
    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("Player"))
        {
            YellowPlatform.SetActive(true);
            GreenPlatform.SetActive(false);
            Destroy(gameObject);

        }
    }
}
