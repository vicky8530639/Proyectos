using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayLevel : MonoBehaviour
{
    
    public string sceneName;
    
    public void LoadScene()
    {
        SceneManager.LoadScene(sceneName);
        // Restablecer el estado del juego
        GameMaster.ResetGame();

        // Reiniciar la posici�n del jugador
        if (GameMaster.instance != null)
        {
            GameMaster.instance.lastCheckPointPos = new Vector2(0, 0); // O la posici�n inicial deseada
        }


    }
    public void Continue()
    {
        // No es una nueva partida
        GameMaster.isNewGame = false;

        SceneManager.LoadScene(PlayerPrefs.GetInt("SavedScene"));

    }
}
