using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameMaster : MonoBehaviour
{
    public static GameMaster instance;
    public Vector2 lastCheckPointPos;
    public static bool isNewGame = true; // Bandera para identificar si es una nueva partida


    private void Awake()
    {
        if(instance == null)
        {
            instance = this;
            DontDestroyOnLoad(gameObject); // Cambiado de instance a gameObject

        }
        else
        {
            Destroy(gameObject);
        }
    }
    public static void ResetGame()
    {
        isNewGame = true;
    }
}
