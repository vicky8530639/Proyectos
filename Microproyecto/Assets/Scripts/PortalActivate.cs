using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PortalActivate : MonoBehaviour
{
    public GameObject Objetollave;
    public GameObject Portal;


    public GameObject portalMensaje;


    public AudioSource audioSource;
    public AudioClip pickUp;
    private void Start()
    {
        Portal.SetActive(false);
        portalMensaje.SetActive(false);
    }
    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("Player"))
        {
            audioSource.PlayOneShot(pickUp);
            portalMensaje.SetActive(true);
            Portal.SetActive(true);
            Destroy(Objetollave);
            Destroy(portalMensaje, 5f);
        }
    }
}
