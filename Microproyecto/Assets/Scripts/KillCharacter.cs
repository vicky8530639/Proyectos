using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class KillCharacter : MonoBehaviour
{
    private Rigidbody2D rb;
    public Animator anim;
    private void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        //anim = GetComponent<Animator>();
    }
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.F))
        {
            rb.bodyType = RigidbodyType2D.Static;
            anim.SetTrigger("Dead");
        }
    }
    private void RestartLevel()
    {

        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }
}
