using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InvisibleBlock : MonoBehaviour
{
    public GameObject bloque;
    public GameObject padreBloque;
    // Start is called before the first frame update
    void Start()
    {
        bloque.SetActive(false);
        padreBloque.SetActive(true);
    }

    // Update is called once per frame
    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("Player"))
        {
            bloque.SetActive(true);
            padreBloque.SetActive(false);
            
        }
    }
}
