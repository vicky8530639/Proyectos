using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CepoTrap : MonoBehaviour
{
    public Animator anim;
    public AudioSource audioSource;
    public AudioClip trap;
    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("Player"))
        {
            audioSource.PlayOneShot(trap);
            anim.SetTrigger("cepo");
        }
            
    }
}
