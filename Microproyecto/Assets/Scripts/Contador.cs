using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class Contador : MonoBehaviour
{
    public static Contador instance;

    public TMP_Text cassetsText;
    public int valueCasset = 0;

    private void Awake()
    {
        instance = this;
    }
    // Start is called before the first frame update
    void Start()
    {
        cassetsText.text = " " + valueCasset.ToString();
    }

    public void IncreaseCassets(int v)
    {
        valueCasset += v;
        cassetsText.text= " " + valueCasset.ToString();
    }
}
