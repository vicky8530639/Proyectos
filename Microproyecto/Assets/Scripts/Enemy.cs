using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    public int health = 10;
    //public GameObject deathEffect;
    public Animator anim;

    public AudioSource audioSource;
    public AudioClip enemyDeath;

    public void TakeDamage (int damage)
    {
        health -= damage;
        if (health <= 0)
        {
            Die();
        }
    }
    void Die()
    {
        anim.SetTrigger("death_enemy");
        //Instantiate(deathEffect, transform.position, Quaternion.identity);
        Evaporate();
        
    }
    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("Bullet"))
        {
            audioSource.PlayOneShot(enemyDeath);
            anim.SetTrigger("death_enemy");
            
        }
    }
    void Evaporate()
    {
        Destroy(gameObject);
    }

}
