using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CambioDia : MonoBehaviour
{
    public GameObject dia;
    public GameObject noche;
    // Start is called before the first frame update
    void Start()
    {
        dia.SetActive(true);
        noche.SetActive(false);
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        dia.SetActive(false);
        noche.SetActive(true);
    }
}
