using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TarodevController;

public class bullet : MonoBehaviour
{
    public float speed = 20f;
    public int damage = 40;
    public Rigidbody2D rb;

    private SpriteRenderer player;

    void Start()
    {
        player = GameObject.FindWithTag("PlayerSprite").GetComponent<SpriteRenderer>();

        if (!player.flipX)  {

            rb.velocity = transform.right * speed;

        } else
        {

            rb.velocity = -transform.right * speed;
        }
        
    }

    private void OnTriggerEnter2D(Collider2D hitInfo)
    {
        
        if (hitInfo.CompareTag("Enemy"))
        {
            Enemy enemy = hitInfo.GetComponent<Enemy>();
            enemy.TakeDamage(damage);
            Destroy(gameObject);
        }
        
    }
}
