using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CheckPoint : MonoBehaviour
{
    public Animator anim;
    private GameMaster gm;
    public AudioSource audioSource;
    public AudioClip Save;
    private void Start()
    {
        gm = GameObject.FindGameObjectWithTag("GM").GetComponent<GameMaster>();
    }
    private void OnTriggerEnter2D(Collider2D other)
    {
        if(other.CompareTag("Player"))
        {
            audioSource.PlayOneShot(Save);
            anim.SetTrigger("Save");
            gm.lastCheckPointPos = transform.position;
        } 
    }
}
