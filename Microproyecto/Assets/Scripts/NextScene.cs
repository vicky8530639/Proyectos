using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class NextScene : MonoBehaviour
{
    public int nextScene;

    


    // Start is called before the first frame update
    void Start()
    {
        nextScene = SceneManager.GetActiveScene().buildIndex + 1;
    }


    public void OnTriggerEnter2D(Collider2D collision)
    {
        if(SceneManager.GetActiveScene().buildIndex == 7)
        {
            Debug.Log("WIN!!!");
        }
        else
        {
            if (collision.gameObject.tag == "Player")
            {

                SceneManager.LoadScene(nextScene);
                

                if (nextScene > PlayerPrefs.GetInt("nivel"))
                {
                    PlayerPrefs.SetInt("nivel", nextScene);

                }
            }
        }
       
    }
    
    public void Reset()
    {
        PlayerPrefs.DeleteAll();        
    }
}
