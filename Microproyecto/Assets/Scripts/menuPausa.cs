using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


public class menuPausa : MonoBehaviour
{
    public GameObject botonPausa;
    public GameObject mPausa;
    public GameObject MenuOpciones;
    
    private bool juegoPausado = false;

    public string sceneName;
    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (juegoPausado)
            {
                Reanudar();
            }
            else
            {
                Pausa();
            }
        }
    }
    public void Pausa()
    {
        juegoPausado = true;
        MenuOpciones.SetActive(false);
        Time.timeScale = 0f;
        botonPausa.SetActive(false);
        mPausa.SetActive(true);
    }
    public void Reanudar()
    {
        juegoPausado = false;
        MenuOpciones.SetActive(false);
        Time.timeScale = 1f;
        botonPausa.SetActive(true);
        mPausa.SetActive(false);
    }
    public void Opciones()
    {
        juegoPausado = false;
        MenuOpciones.SetActive(true);
        Time.timeScale = 0f;
        botonPausa.SetActive(false);
        mPausa.SetActive(false);
    }
    
    public void Volver()
    {
        Time.timeScale = 1f;
        PlayerPrefs.SetInt("SavedScene", SceneManager.GetActiveScene().buildIndex);
        SceneManager.LoadScene("Menu Principal");
    }

    public void Reiniciar()
    {
        Time.timeScale = 1f;
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
        PlayerPrefs.DeleteAll();
    }
}
