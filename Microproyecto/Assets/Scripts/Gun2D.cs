using System.Collections;
using System.Collections.Generic;

using UnityEngine;

public class Gun2D : MonoBehaviour
{
    public Transform firePoint;
    public GameObject bulletprefab;
    private SpriteRenderer player;
    public float spawnDistance;


    void Update()
    {
        if (Input.GetKeyDown(KeyCode.E))
        {
            Shoot();
        }
    }

    void Shoot()
    {
        player = GameObject.FindWithTag("PlayerSprite").GetComponent<SpriteRenderer>();
        float Xpos;

        if (!player.flipX)
        {
            Xpos = firePoint.position.x + spawnDistance;
           

        }
        else
        {
            Xpos = firePoint.position.x - spawnDistance;
            

        }

        Vector3 position = new Vector3(Xpos, firePoint.position.y, firePoint.position.z);

        Instantiate(bulletprefab, position, firePoint.rotation);
    }
}
