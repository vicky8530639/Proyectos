using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class VidaJugagor : MonoBehaviour
{
    private Rigidbody2D rb;
    public Animator anim;
    public AudioSource audioSource;
    public AudioClip death;


    bool jugadorMuerto = false;
    float tiempoParaReinicio = 1.5f;
    private void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        //anim = GetComponent<Animator>();
    }
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Trap"))
        {
            Die();
        }
    }
    private void Die()
    {
        jugadorMuerto = true;
        rb.bodyType = RigidbodyType2D.Static;
        audioSource.PlayOneShot(death);
        anim.SetTrigger("Dead");
    }
    private void Update()
    {
        if (jugadorMuerto)
        {
            StartCoroutine(ReiniciarDespuesDeTiempo(tiempoParaReinicio));
        }
    }
    IEnumerator ReiniciarDespuesDeTiempo(float tiempo)
    {
        yield return new WaitForSeconds(tiempo);
        RestartLevel();
    }

    private void RestartLevel()
    {
        
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }
}
