﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    public float jumpForce;
    public float playerSpeed;
    public Rigidbody2D playerRb;
    public Animator playerAnim;

    public AudioSource jumpAudio;

    public bool playerLeft;
    public bool playerRight;

    public bool isGrounded;

    // Start is called before the first frame update
    void Start()
    {
        playerRb = GetComponent<Rigidbody2D>();
        playerLeft = false;
        playerRight = false;
        isGrounded = true;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.RightArrow) && !playerRight)
        {
            //Movimiento constante a la derecha
            playerRb.velocity = new Vector2(0, playerRb.velocity.y);
            playerRb.AddForce(Vector2.right.normalized * playerSpeed, ForceMode2D.Force);
            this.transform.localScale = new Vector3(1, 1, 1);
            playerLeft = false;
            playerRight = true;
        }
        else if (Input.GetKeyDown(KeyCode.LeftArrow) && !playerLeft)
        {
            //Movimiento constante a la izquierda
            playerRb.velocity = new Vector2(0, playerRb.velocity.y);
            playerRb.AddForce(Vector2.left.normalized * playerSpeed, ForceMode2D.Force);
            this.transform.localScale = new Vector3(-1, 1, 1);
            playerRight = false;
            playerLeft = true;
        }

        if (Input.GetKeyDown(KeyCode.UpArrow) && isGrounded)
        {
            //Salto hacia la direcci�n que est� yendo
            //jumpAudio.Play();
            playerAnim.SetBool("isJumping", true);
            playerRb.AddForce(Vector2.up * jumpForce, ForceMode2D.Force);
            isGrounded = false;
        }
        else if (!isGrounded)
        {

        }
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Ground"))
        {
            playerAnim.SetBool("isJumping", false);
            isGrounded = true;
        }

    }
}

