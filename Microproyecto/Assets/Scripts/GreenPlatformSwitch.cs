using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GreenPlatformSwitch : MonoBehaviour
{
    public GameObject GreenPlatform;
    public GameObject YellowPlatform;
    void Start()
    {
        GreenPlatform.SetActive(false);
    }

    // Update is called once per frame
    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("Player"))
        {
            YellowPlatform.SetActive(false);
            GreenPlatform.SetActive(true);
            Destroy(gameObject);

        }
    }
}
